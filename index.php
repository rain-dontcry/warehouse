<?php include './includes/header.php' ?>
<body class="mat-typography">
<?php include './includes/navbar.php' ?>
  <div id="wrapper" class="wrapper">
  <?php include './includes/sidebar.php' ?>
    <div class="content">
      <div class="breadcrumb">
        <?php include './includes/breadcrumb.php';?>
        <?php
          $breadcrumb = new breadcrumb();
          echo $breadcrumb->build(array());
        ?>
        <div class="search-form">
          <form [formGroup]="searchFrm" (ngSubmit)="onSubmit(searchFrm.value)">
            <input type="text" formControlName="searchKey">
            <span class="ico-search fa fa-search"></span>
          </form>
        </div>
      </div>
      <div class="main-content"></div>
    </div>
  </div>
</body>

</html>