<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// include database and object files
include_once '../../config/database.php';
include_once '../../objects/user/login.php';

// instantiate database and login object
$database = new Database();
$db = $database->getConnection();

// initialize object
$login = new Login($db);

$request_body = file_get_contents('php://input');
$data = json_decode($request_body);

foreach ($data as $key => $value) {
  $login->$key = $value;
}

// query logins
$stmt = $login->init();
$num = $stmt->rowCount();

// check if more than 0 record found
if ($num == 1) {
  $categories_arr = array();
  while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
    extract($row);

    $login_item = array(
			"id" => $id,
      "firstname" => $firstname,
      "lastname" => $lastname,
      "address" => $address,
      "birthday" => $birthday,
      "phone" => $phone,
      "email" => $email,
      "roles" => $roles,
      "avatar" => $avatar,
      "username" => $username
    );
    array_push($categories_arr, $login_item);
  }
  echo json_encode($login_item);
} else {
  echo 0;
}
?>