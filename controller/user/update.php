<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=utf-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// get database connection
include_once '../../config/database.php';

// instantiate update object
include_once '../../objects/user/update.php';

$database = new Database();
$db = $database->getConnection();

$update = new Update($db);

$update->id = $_POST['id'];
$update->firstname = $_POST['firstname'];
$update->lastname = $_POST['lastname'];
$update->address = $_POST['address'];
$update->birthday = $_POST['birthday'];
$update->phone = $_POST['phone'];
$update->email = $_POST['email'];
$update->password = isset($_POST['password']) ? $_POST['password'] : '';
$update->re_password = isset($_POST['re_password']) ? $_POST['re_password'] : '';
$update->modified = date('Y-m-d H:i:s');

// random image name
function randomString($length = 4) {
	$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	$charactersLength = strlen($characters);
	$randomString = '';
	for ($i = 0; $i < $length; $i++) {
		$randomString .= $characters[rand(0, $charactersLength - 1)];
	}
	return $randomString;
}

if($update->password != $update->re_password) {
  echo json_encode('{"res": "0"}');
  die;
} else {
	if (!empty($_FILES)) {
		// Upload all of image to directory
		$tempPath = $_FILES['avatar']['tmp_name'];
		$dirTmp = $_SERVER['DOCUMENT_ROOT'] . '/uploads/';
		$dirTmp = str_replace('//', '/', $dirTmp);
		$direcTime = date("Y") . "/" . date("m");
	
		if (!file_exists($dirTmp . $direcTime)) {
			mkdir($dirTmp . $direcTime, 0777, true);
		}

		$imgName = $_FILES['avatar']['name'];
		$imgName = strtolower(randomString(4) . '_' . $imgName);
		$uploadPath = $dirTmp . $direcTime . DIRECTORY_SEPARATOR . $imgName;
	
		if (move_uploaded_file($tempPath, $uploadPath)) {
			// setup parameter to save to database
			$update->avatar = '/uploads/' . $direcTime . '/' . $imgName;
	
			// query getlists
			if($update->init()) {
				echo json_encode('{"res": "1"}');
			} else {
				echo json_encode('{"res": "0"}');
			}
		} else {
			echo json_encode('{"res": "2"}');
		}
	} else {
		// setup parameter to save to database
		$update->avatar = '';

		// query getlists
		if($update->init()) {
			echo json_encode('{"res": "1"}');
		} else {
			echo json_encode('{"res": "0"}');
		}
	}
}
?>