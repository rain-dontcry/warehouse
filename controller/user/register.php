<?php
// required headers
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// get database connection
include_once '../../config/database.php';
include_once '../../objects/user/register.php';

$database = new Database();
$db = $database->getConnection();

$register = new Register($db);

$register->firstname = $_POST['firstname'];
$register->lastname = $_POST['lastname'];
$register->address = $_POST['address'];
$register->birthday = $_POST['birthday'];
$register->phone = $_POST['phone'];
$register->email = $_POST['email'];
$register->username = $_POST['username'];
$register->password = $_POST['password'];
$register->re_password = $_POST['re_password'];
$register->roles = $_POST['roles'];
$register->created = date('Y-m-d H:i:s');
$register->modified = date('Y-m-d H:i:s');

// random image name
function randomString($length = 4) {
	$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	$charactersLength = strlen($characters);
	$randomString = '';
	for ($i = 0; $i < $length; $i++) {
		$randomString .= $characters[rand(0, $charactersLength - 1)];
	}
	return $randomString;
}

if($register->password != $register->re_password) {
  echo json_encode('{"res": "0"}');
  die;
} else {
	if (!empty($_FILES)) {
		// Upload all of image to directory
		$tempPath = $_FILES['avatar']['tmp_name'];
		$dirTmp = $_SERVER['DOCUMENT_ROOT'] . '/uploads/';
		$dirTmp = str_replace('//', '/', $dirTmp);
		$direcTime = date("Y") . "/" . date("m");
	
		if (!file_exists($dirTmp . $direcTime)) {
			mkdir($dirTmp . $direcTime, 0777, true);
		}

		$imgName = $_FILES['avatar']['name'];
		$imgName = strtolower(randomString(4) . '_' . $imgName);
		$uploadPath = $dirTmp . $direcTime . DIRECTORY_SEPARATOR . $imgName;
	
		if (move_uploaded_file($tempPath, $uploadPath)) {
			// setup parameter to save to database
			$register->avatar = '/uploads/' . $direcTime . '/' . $imgName;
	
			// insert the register
			if ($register->init()) {
				echo json_encode('{"res": "1"}');
			} else {
				echo json_encode('{"res": "0"}');
			}
		} else {
			echo json_encode('{"res": "0"}');
		}
	} else {
		// setup parameter to save to database
		$register->avatar = '';

		// insert the register
		if ($register->init()) {
			echo json_encode('{"res": "1"}');
		} else {
			echo json_encode('{"res": "0"}');
		}
	}
}

?>