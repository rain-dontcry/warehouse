<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=utf-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// get database connection
include_once '../../config/database.php';

// instantiate insert object
include_once '../../objects/proccess/insert.php';

$database = new Database();
$db = $database->getConnection();

$insert = new Insert($db);

$insert->username = isset($_POST['username']) ? $_POST['username'] : '';
$insert->location = isset($_POST['location']) ? $_POST['location'] : '';
$insert->status = isset($_POST['status']) ? $_POST['status'] : '';

$insert->created = date('Y-m-d H:i:s');
$insert->modified = date('Y-m-d H:i:s');

// insert the insert
if ($insert->init()) {
	echo json_encode('{"res": "1"}');
} else {
	echo json_encode('{"res": "0"}');
}
?>