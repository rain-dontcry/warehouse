<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// include database and object files
include_once '../../config/database.php';
include_once '../../objects/proccess/getlist.php';

// instantiate database and getlist object
$database = new Database();
$db = $database->getConnection();

// initialize object
$getlist = new Getlist($db);

// query getlists
$stmt = $getlist->init();
$num = $stmt->rowCount();

// check if more than 0 record found
if ($num > 0) {
	$proccess_arr = array();
	while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
    extract($row);

		$getlist_item = array(
      "id" => $id,
      "username" => $username,
      "location" => $location,
      "status" => $status
		);

		array_push($proccess_arr, $getlist_item);
	}

	echo json_encode($proccess_arr);
} else {
	return 0;
}
?>