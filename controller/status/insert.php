<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// get database connection
include_once '../../config/database.php';

// instantiate insert object
include_once '../../objects/status/insert.php';

$database = new Database();
$db = $database->getConnection();

$insert = new Insert($db);

$insert->status = $_POST['status'];
$insert->order = $_POST['order'];
$insert->created = date('Y-m-d H:i:s');
$insert->modified = date('Y-m-d H:i:s');

if ($insert->init()) {
  echo json_encode('{"res": "1"}');
} else {
  echo json_encode('{"res": "0"}');
}

?>