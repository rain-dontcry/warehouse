<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// include database and object files
include_once '../../config/database.php';
include_once '../../objects/status/getlist.php';

// instantiate database and getlist object
$database = new Database();
$db = $database->getConnection();

// initialize object
$getlist = new Getlist($db);

// query getlists
$stmt = $getlist->init();
$num = $stmt->rowCount();

// check if more than 0 record found
if ($num > 0) {
	$categories_arr = array();
	while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
    extract($row);

		$getlist_item = array(
         "id" => $id,
         "status" => $status,
         "order" => $order,
         "created" => $created,
         "modified" => $modified
		);

		array_push($categories_arr, $getlist_item);
	}

	echo json_encode($categories_arr);
} else {
	return 0;
}
?>