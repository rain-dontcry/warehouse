<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=utf-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// get database connection
include_once '../../config/database.php';

// instantiate update object
include_once '../../objects/status/update.php';

$database = new Database();
$db = $database->getConnection();

$update = new Update($db);

$update->id = $_POST['id'];
$update->status = isset($_POST['status']) ? $_POST['status'] : '';
$update->order = isset($_POST['order']) ? $_POST['order'] : '';
$update->modified = date('Y-m-d H:i:s');

// update the update
if ($update->init()) {
	echo json_encode('{"res": "1"}');
} else {
	echo json_encode('{"res": "0"}');
}
?>