<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=utf-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// get database connection
include_once '../../config/database.php';

// instantiate update object
include_once '../../objects/product/update.php';

$database = new Database();
$db = $database->getConnection();

$update = new Update($db);

$update->id = $_POST['id'];
$update->barcode = isset($_POST['barcode']) ? $_POST['barcode'] : '';
$update->sku = isset($_POST['sku']) ? $_POST['sku'] : '';
$update->name = isset($_POST['name']) ? $_POST['name'] : '';
$update->location = isset($_POST['location']) ? $_POST['location'] : '';
$update->new_location = isset($_POST['new_location']) ? $_POST['new_location'] : '';
$update->onhand = isset($_POST['onhand']) ? $_POST['onhand'] : '';
$update->allocated = isset($_POST['allocated']) ? $_POST['allocated'] : '';
$update->count = isset($_POST['count']) ? $_POST['count'] : '';
$update->qty_return = isset($_POST['qty_return']) ? $_POST['qty_return'] : '';
$update->modified = date('Y-m-d H:i:s');

// update the update
if ($update->init()) {
	echo json_encode('{"res": "1"}');
} else {
	echo json_encode('{"res": "0"}');
}
?>