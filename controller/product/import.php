<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=utf-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// get database connection
include_once '../../config/database.php';

// instantiate insert object
include_once '../../objects/product/insert.php';

$database = new Database();
$db = $database->getConnection();

$insert = new Insert($db);

$insert->barcode = $_POST['barcode'];
$insert->sku = $_POST['sku'];
$insert->name = $_POST['name'];
$insert->location = $_POST['location'];
$insert->onhand = $_POST['onhand'];
$insert->allocated = $_POST['allocated'];
$insert->count = $_POST['count'];
$insert->qty_return = $_POST['qty_return'];
$insert->created = date('Y-m-d H:i:s');
$insert->modified = date('Y-m-d H:i:s');

// insert the insert
if ($insert->init()) {
	echo json_encode('{"res": "1"}');
} else {
	echo json_encode('{"res": "0"}');
}
?>