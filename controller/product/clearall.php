<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// include database and object files
include_once '../../config/database.php';
include_once '../../objects/product/clearall.php';

// instantiate database and clearall object
$database = new Database();
$db = $database->getConnection();

// initialize object
$clearall = new Clearall($db);

// query clearalls
// insert the register
if ($clearall->init()) {
	echo json_encode('{"res": "1"}');
} else {
	echo json_encode('{"res": "0"}');
}

?>