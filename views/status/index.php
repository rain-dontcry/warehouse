<?php include '../../includes/header.php' ?>

<body class="mat-typography">
  <?php include '../../includes/navbar.php' ?>
  <div id="wrapper" class="wrapper">
    <?php include '../../includes/sidebar.php' ?>
    <div class="content">
      <div class="breadcrumb">
        <?php include '../../includes/breadcrumb.php';?>
        <?php
          $breadcrumb = new breadcrumb();
          echo $breadcrumb->build(array(
            'Trạng thái kiểm kho' => '/views/proccess/'
          ));
        ?>
        <div class="search-form">
          <form [formGroup]="searchFrm" (ngSubmit)="onSubmit(searchFrm.value)">
            <input type="text" name="searchKey">
            <span class="ico-search fa fa-search"></span>
          </form>
        </div>
      </div>
      <div class="main-content">
        <div class="page-title">
          <h1>Trạng thái kiểm kho</h1>
          <button type="button" class="btn bg-success text-white js_sp_add pc_hide"><span class="fa fa-plus"></span>&nbsp;Thêm trạng thái</button>
        </div>
        
        <div class="page-wrapper">
          <div class="page-content">
            <div class="row">
              <div class="col-4 sp_hide form_modal">
                <form id="product-frm" class="js-binding-frm" data-action="../../controller/status/insert.php" data-update="../../controller/status/update.php">
                  <button type="button" class="pc_hide md_close">Close</button>
                  <input type="hidden" name="id" id="id" class="form-control" placeholder="id" />
                  <div class="form-group">
                    <label for="status">Trạng thái</label>
                    <input type="text" name="status" id="status" class="form-control" placeholder="Trạng thái" required />
                  </div>
                  <div class="form-group">
                    <label for="order">Thứ tự</label>
                    <input type="number" name="order" id="order" class="form-control" placeholder="Thứ tự" required />
                  </div>
                  <div class="form-group grp-submit sp_clear">
                    <button type="submit" class="btn btn-primary btn-submit">
                      <span class="fa fa-plus"></span>
                      Thêm trường mới
                    </button>
                    <button class="btn btn-close-submit" type="button">
                      <span class="fa fa-times"></span>
                      Hủy bỏ
                    </button>
                  </div>
                </form>
              </div>
              <div class="col-6">
                <div class="product-list">
                  <table class="product-list-tbl js-getlist display" data-list="../../controller/status/getlist.php">
                    <thead>
                      <tr>
                        <th>Trạng thái</th>
                        <th>Thứ tự</th>
                        <th class="col_edit">Edit</th>
                      </tr>
                    </thead>
                    <tbody></tbody>
                  </table>
                  <script id="theTmpl" type="text/x-jsrender">
                    <tr data-edit="{`id`:`{{:id}}`,`status`:`{{:status}}`}">
                      <td>{{:status}}</td>
                      <td>{{:order}}</td>
                      <td class="center">
                        <div class="tbl_button">
                          <span class="btn-table btn-edit fa fa-pencil-square-o" aria-hidden="true" data-edit="{`id`:`{{:id}}`,`status`:`{{:status}}`}"></span>
                          <span class="btn-table btn-delete fa fa-trash-o" aria-hidden="true" data-delete="../../controller/status/delete.php?id={{:id}}&name={{:status}}"></span>
                        </div>
                      </td>
                    </tr>
                  </script>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="copyright_info">
        <span class="count_timing">00:00:00 00/00/0000</span>
        <span>copyright © (phuoc nhi) 2020</span>
      </div>
    </div>
  </div>

  <div class="import_loading">
    <div class="loading_container">
      <div class="loader">Loading...</div>
      <span class="status_import">Loading...</span>
    </div>
  </div>

  <script>
  if(!adminRoles()) {
    window.location.replace("/");
  }
  $(document).ready(function() {
    $('.import_loading').fadeIn(function() {
      var _table = $('.js-getlist');
      getList(_table);
      httpRequest($("#product-frm"), 'POST', _table);
    });
  });
  </script>
</body>

</html>