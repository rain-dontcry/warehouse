<?php include '../../includes/header.php' ?>
<div class="root__container">
  <div class="main-container__bg">
    <div class="main__container">
      <div class="login-card">
        <main class="login-content">
          <span class="login-header">Đăng ký</span>
          <form class="login-form" id="register-frm" data-action="/warehouse/controller/user/register.php">
            <input type="text" name="username" placeholder="User name" class="login-input" />
            <input type="password" name="password" placeholder="Password" class="login-input" />
            <input type="password" name="re_password" placeholder="Confirm Password" class="login-input" />
            <input type="submit" class="login-btn btn-disabled" value="Đăng ký" />
          </form>
          <div class="notive">
            <span></span>
          </div>
          <div class="signup-link-wrapper">
            <span class="signup-notice">Bạn đã có tài khoản?</span>
            <a href="/warehouse/views/user/login.php" class="signup-link">Đăng nhập</a>
          </div>
        </main>
        <aside class="login-aside">
          <div class="login-aside-overlay"></div>
          <h1 class="login-welcome-text">Welcome Back!</h1>
          <hr class="login-aside-hr">
        </aside>
      </div>
    </div>
  </div>
</div>
<script>
  // this is the id of the form
  httpRequest($("#register-frm"), 'POST', null);
  if (!checkCookie('username')) {
    window.location.replace("../views/user/login.php");
  }
</script>