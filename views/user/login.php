<?php include '../../includes/header.php' ?>
<div class="root__container">
  <div class="main-container__bg">
    <div class="main__container">
      <div class="login-card">
        <main class="login-content">
          <span class="login-header">Đăng nhập</span>
          <form class="login-form" id="login-frm" action="../../controller/user/login.php">
            <span class="notice_correct">User name or password incorrect</span>
            <input type="text" name="username" placeholder="User name" class="login-input" autocomplete="off" required/>
            <input type="password" name="password" placeholder="Password" class="login-input" autocomplete="on" required/>
            <input type="submit" class="login-btn" value="Đăng nhập" />
          </form>
          <div class="signup-link-wrapper">
            <span class="signup-notice">Bạn chưa có tài khoản?</span>
            <a href="/views/user/register.php" class="signup-link">Đăng ký</a>
          </div>
        </main>
        <aside class="login-aside">
          <div class="login-aside-overlay"></div>
          <h1 class="login-welcome-text">Welcome Back!</h1>
        </aside>
      </div>
    </div>
  </div>
</div>
<script>
  // this is the id of the form
  $("#login-frm").submit(function(e) {
    e.preventDefault(); // avoid to execute the actual submit of the form.
    var form = $(this);
    var url = form.attr('action');
    var dataArr = form.serializeArray();
    var dataStr = '{';
    for (let i = 0; i < dataArr.length; i++) {
      if(i == dataArr.length - 1) {
        dataStr += '"' + dataArr[i].name + '":"' + dataArr[i].value + '"';
      } else {
        dataStr += '"' + dataArr[i].name + '":"' + dataArr[i].value + '",';
      }
    }
    dataStr += '}';

    $.ajax({
      type: "POST",
      url: url,
      contentType: "application/json; charset=utf-8",
      data: dataStr, // serializes the form's elements.
      success: function(data)
      {
        if(data) {
          setCookie('id_user', data.id.toString(), 1);
          setCookie('username', data.username.toString(), 1);
          setCookie('firstname', data.firstname ? data.firstname.toString() : '', 1);
          setCookie('lastname', data.lastname ? data.lastname.toString() : '', 1);
          setCookie('avatar', data.avatar ? data.avatar.toString() : '', 1);
          setCookie('address', data.address ? data.address.toString() : '', 1);
          setCookie('birthday', data.birthday ? data.birthday.toString() : '', 1);
          setCookie('phone', data.phone ? data.phone.toString() : '', 1);
          setCookie('email', data.email ? data.email.toString() : '', 1);
          setCookie('roles', data.roles.toString(), 1);
          window.location.replace("/");
        } else {
          $('.notice_correct').fadeIn();
        }
      }
    });
  });
</script>