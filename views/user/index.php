<?php include '../../includes/header.php' ?>

<body class="mat-typography">
  <?php include '../../includes/navbar.php' ?>
  <div id="wrapper" class="wrapper">
    <?php include '../../includes/sidebar.php' ?>
    <div class="content">
      <div class="breadcrumb">
        <?php include '../../includes/breadcrumb.php';?>
        <?php
          $breadcrumb = new breadcrumb();
          echo $breadcrumb->build(array(
            'Users' => '/views/user/'
          ));
        ?>
        <div class="search-form">
          <form [formGroup]="searchFrm" (ngSubmit)="onSubmit(searchFrm.value)">
            <input type="text" name="searchKey">
            <span class="ico-search fa fa-search"></span>
          </form>
        </div>
      </div>
      <div class="main-content">
        <div class="page-title">
          <h1>Người dùng</h1>
        </div>
        <div class="page-wrapper">
          <div class="page-content">
            <div class="row">
              <div class="col-3 sp_hide form_modal">
                <form id="user-frm" class="js-binding-frm" data-action="../../controller/user/register.php" data-update="../../controller/user/update.php">
                  <button type="button" class="pc_hide md_close">Close</button>
                  <input type="hidden" name="id" id="id" class="form-control" placeholder="id" />
                  <div class="form-group">
                    <label for="username">Tên đăng nhập</label>
                    <input type="text" name="username" id="username" class="form-control" placeholder="Tên đăng nhập" required />
                  </div>
                  <div class="form-group">
                    <label for="roles">Quyền người dùng</label>
                    <select name="roles" id="roles" class="form-control" required>
                      <option value="">-- Quyền --</option>
                      <option value="admin">Admin</option>
                      <option value="user">User</option>
                    </select>
                  </div>
                  <div class="form-group sp50">
                    <label for="password">Mật khẩu</label>
                    <input type="password" name="password" id="password" class="form-control" placeholder="Mật khẩu" required />
                  </div>
                  <div class="form-group sp50 sp50_last">
                    <label for="re_password">Nhập lại mật khẩu</label>
                    <input type="password" name="re_password" id="re_password" class="form-control" placeholder="Nhập lại mật khẩu" required />
                  </div>
                  <div class="form-group sp50">
                    <label for="firstname">First name</label>
                    <input type="text" name="firstname" id="firstname" class="form-control" placeholder="First name" />
                  </div>
                  <div class="form-group sp50 sp50_last">
                    <label for="lastname">Last name</label>
                    <input type="text" name="lastname" id="lastname" class="form-control" placeholder="Last name" />
                  </div>
                  <div class="form-group sp_clear">
                    <label>Ảnh đại diện</label>
                    <div class="grp-image">
                      <div class="avatar_file_up">
                        <img src="../../assets/img/no-image.png" class="img-view" alt="avatar">
                      </div>
                      <label for="avatar" class="custom-file-upload"><i class="fa fa-cloud-upload"></i>Chọn ảnh</label>
                      <input type="file" id="avatar" name="avatar" class="imgInp bg-success text-white" accept="image/png, image/jpeg">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="address">Địa chỉ</label>
                    <input type="text" name="address" id="address" class="form-control" placeholder="Địa chỉ" />
                  </div>
                  <div class="form-group sp50">
                    <label for="birthday">Ngày sinh</label>
                    <input type="date" name="birthday" id="birthday" class="form-control" placeholder="Ngày sinh" />
                  </div>
                  <div class="form-group sp50 sp50_last">
                    <label for="phone">Số điện thoại</label>
                    <input type="text" name="phone" id="phone" class="form-control" placeholder="Số điện thoại" />
                  </div>
                  <div class="form-group sp_clear">
                    <label for="email">Email</label>
                    <input type="email" name="email" id="email" class="form-control" placeholder="Email" />
                  </div>
                  <div class="form-group grp-submit">
                    <button type="submit" class="btn btn-primary btn-submit">
                      <span class="fa fa-plus"></span>
                      Thêm trường mới
                    </button>
                    <button type="button" class="btn btn-close-submit">
                      <span class="fa fa-times"></span>
                      Hủy bỏ
                    </button>
                  </div>
                </form>
              </div>
              <div class="col-7">
                <div class="user-list">
                  <table class="user-list-tbl js-getlist" data-list="../../controller/user/getlist.php">
                    <thead>
                      <tr>
                        <th>Tên đăng nhập</th>
                        <th>Tên hiển thị</th>
                        <th>Avatar</th>
                        <th>Địa chỉ</th>
                        <th>Ngày sinh</th>
                        <th>Số điện thoại</th>
                        <th>Email</th>
                        <th>Quyền</th>
                        <th class="js_roles">Edit</th>
                      </tr>
                    </thead>
                    <tbody></tbody>
                  </table>
                  <script id="theTmpl" type="text/x-jsrender">
                    {{if avatar}}
                      <tr data-edit="{`id`:`{{:id}}`,`username`:`{{:username}}`,`password`:`******`,`re_password`:`******`,`firstname`:`{{:firstname}}`,`lastname`:`{{:lastname}}`,`avatar`:`{{:avatar}}`,`address`:`{{:address}}`,`birthday`:`{{:birthday}}`,`phone`:`{{:phone}}`,`email`:`{{:email}}`,`avatar`:`{{:avatar}}`,`roles`:`{{:roles}}`}">
                    {{else}}
                      <tr data-edit="{`id`:`{{:id}}`,`username`:`{{:username}}`,`password`:`******`,`re_password`:`******`,`firstname`:`{{:firstname}}`,`lastname`:`{{:lastname}}`,`avatar`:`{{:avatar}}`,`address`:`{{:address}}`,`birthday`:`{{:birthday}}`,`phone`:`{{:phone}}`,`email`:`{{:email}}`,`avatar`:`../../assets/img/no-image.png`,`roles`:`{{:roles}}`}">
                    {{/if}}
                      <td>{{:username}}</td>
                      <td>
                          {{if firstname}}
                            {{:firstname}}
                          {{else}}
                            null
                          {{/if}}
                          {{:lastname}}</td>
                      <td class="center">
                        {{if avatar}}
                          <span class="tbl-img-avatar"><img src="{{:avatar}}"></span>
                        {{else}}
                          <span class="tbl-img-avatar"><img src="../../assets/img/no-image.png"></span>
                        {{/if}}
                      </td>
                      <td>{{:address}}</td>
                      <td>{{:birthday}}</td>
                      <td>{{:phone}}</td>
                      <td>{{:email}}</td>
                      {{if roles == 'admin'}}
                        <td class="center"><span class="user_role user_admin">{{:roles}}</span></td>
                      {{else}}
                        <td class="center"><span class="user_role">{{:roles}}</span></td>
                      {{/if}}
                      <td class="center js_roles">
                        <div class="tbl_button">
                          {{if username != 'admin'}}
                            {{if avatar}}
                              <span class="btn-table btn-edit fa fa-pencil-square-o" aria-hidden="true" data-edit="{`id`:`{{:id}}`,`username`:`{{:username}}`,`password`:`******`,`re_password`:`******`,`firstname`:`{{:firstname}}`,`lastname`:`{{:lastname}}`,`avatar`:`{{:avatar}}`,`address`:`{{:address}}`,`birthday`:`{{:birthday}}`,`phone`:`{{:phone}}`,`email`:`{{:email}}`,`avatar`:`{{:avatar}}`,`roles`:`{{:roles}}`}"></span>
                            {{else}}
                              <span class="btn-table btn-edit fa fa-pencil-square-o" aria-hidden="true" data-edit="{`id`:`{{:id}}`,`username`:`{{:username}}`,`password`:`******`,`re_password`:`******`,`firstname`:`{{:firstname}}`,`lastname`:`{{:lastname}}`,`avatar`:`{{:avatar}}`,`address`:`{{:address}}`,`birthday`:`{{:birthday}}`,`phone`:`{{:phone}}`,`email`:`{{:email}}`,`avatar`:`../../assets/img/no-image.png`,`roles`:`{{:roles}}`}"></span>
                            {{/if}}
                            <span class="btn-table btn-delete fa fa-trash-o" aria-hidden="true" data-delete="../../controller/user/delete.php?id={{:id}}&name={{:username}}&avatar={{:avatar}}"></span>
                          {{/if}}
                        </div>
                      </td>
                    </tr>
                  </script>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="copyright_info">
        <span class="count_timing">00:00:00 00/00/0000</span>
        <span>copyright © (phuoc nhi) 2020</span>
      </div>
    </div>
  </div>

  <div class="import_loading">
    <div class="loading_container">
      <div class="loader">Loading...</div>
      <span class="status_import">Loading...</span>
    </div>
  </div>

  <script>
  if(!adminRoles()) {
    window.location.replace("/");
  }
  $(document).ready(function() {
    $('.import_loading').fadeIn(function() {
      var _table = $('.js-getlist');
      getList(_table);
      httpRequest($("#user-frm"), 'POST', _table);
    });
  });
  </script>
</body>

</html>