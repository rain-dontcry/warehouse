<?php include '../../includes/header.php' ?>

<body class="mat-typography">
  <?php include '../../includes/navbar.php' ?>
  <div id="wrapper" class="wrapper">
    <?php include '../../includes/sidebar.php' ?>
    <div class="content">
      <div class="breadcrumb">
        <?php include '../../includes/breadcrumb.php';?>
        <?php
          $breadcrumb = new breadcrumb();
          echo $breadcrumb->build(array(
            'Users profile' => '/views/user/profile.php'
          ));
        ?>
        <div class="search-form">
          <form [formGroup]="searchFrm" (ngSubmit)="onSubmit(searchFrm.value)">
            <input type="text" name="searchKey">
            <span class="ico-search fa fa-search"></span>
          </form>
        </div>
      </div>
      <div class="main-content">
        <div class="page-title">
          <h1>Thông tin cá nhân</h1>
          <button type="button" class="btn bg-success text-white js_update_profile"><span class="fa fa-pencil"></span>&nbsp;Update profile</button>
        </div>
        <div class="page-wrapper">
          <div class="page-content">
            <div class="user-profile">
              <div class="profile-container">
                <div class="row js_user">
                  <div class="avatar_info col-4">
                    <div class="avatar_area">
                      <img src="<?php echo $_COOKIE["avatar"] ? $_COOKIE["avatar"] : "../../assets/img/no-image.png" ?>" alt="avatar">
                    </div>
                    <div class="process_count">
                      <span>Đang đếm: F9</span>
                      <div class="status_count">
                        <span>Trạng thái: Vừa giao việc</span>
                        <input type="button" class="btn js_count_status" value="Xác nhận">
                      </div>
                    </div>
                  </div>
                  <div class="profile_info col-6">
                    <ul>
                      <li><span class="fa fa-user-o"></span>User name: <?php echo isset($_COOKIE["username"]) ? $_COOKIE["username"] : "" ?></li>
                      <li><span class="fa fa-address-card-o"></span>Full name: <?php echo isset($_COOKIE["firstname"]) ? $_COOKIE["firstname"] . " " : ""; echo isset($_COOKIE["lastname"]) ? $_COOKIE["lastname"] : ""; ?></li>
                      <li><span class="fa fa-address-book-o"></span>Address: <?php echo isset($_COOKIE["address"]) ? $_COOKIE["address"] : "" ?></li>
                      <li><span class="fa fa-calendar-o"></span>Birthday: <?php echo isset($_COOKIE["birthday"]) ? explode(" ", $_COOKIE["birthday"])[0] : "" ?></li>
                      <li><span class="fa fa-phone"></span>Phone: <?php echo isset($_COOKIE["phone"]) ? $_COOKIE["phone"] : "" ?></li>
                      <li><span class="fa fa-envelope-o"></span>Email: <?php echo isset($_COOKIE["email"]) ? $_COOKIE["email"] : "" ?></li>
                      <li class="social_info">
                        <a href="#"><span class="fa fa-facebook-square ico-facebook"></span></a>
                        <a href="#"><span class="fa fa-instagram ico-instagram"></span></a>
                        <a href="#"><span class="fa fa-apple ico-apple"></span></a>
                        <a href="#"><span class="fa fa-twitter ico-twitter"></span></a>
                      </li>
                    </ul>
                  </div>
                </div>
                <div class="frm_edit_profile js_edit_profile">
                  <form id="user-frm" class="js-binding-frm" data-action="../../controller/user/update.php">
                    <button type="button" class="md_close">Close</button>
                    <input type="hidden" name="id" id="id" class="form-control" placeholder="id" value="<?php echo isset($_COOKIE["id_user"]) ? $_COOKIE["id_user"] : "" ?>" />
                    <div class="form-group sp50">
                      <label for="firstname">First name</label>
                      <input type="text" name="firstname" id="firstname" class="form-control" placeholder="First name" value="<?php echo isset($_COOKIE["firstname"]) ? $_COOKIE["firstname"] : "" ?>" />
                    </div>
                    <div class="form-group sp50 sp50_last">
                      <label for="lastname">Last name</label>
                      <input type="text" name="lastname" id="lastname" class="form-control" placeholder="Last name" value="<?php echo isset($_COOKIE["lastname"]) ? $_COOKIE["lastname"] : "" ?>"/>
                    </div>
                    <div class="form-group sp50">
                      <label for="password">Mật khẩu mới</label>
                      <input type="password" name="password" id="password" class="form-control" placeholder="Mật khẩu" />
                    </div>
                    <div class="form-group sp50 sp50_last">
                      <label for="re_password">Nhập lại mật khẩu mới</label>
                      <input type="password" name="re_password" id="re_password" class="form-control" placeholder="Nhập lại mật khẩu" />
                    </div>
                    <div class="form-group sp_clear">
                      <label>Ảnh đại diện</label>
                      <div class="grp-image">
                        <div class="avatar_file_up">
                          <?php
                            if($_COOKIE["avatar"]) {
                              echo '<img src="' . $_COOKIE["avatar"] . '" class="img-view" alt="avatar">';
                            } else {
                              echo '<img src="../../assets/img/no-image.png" class="img-view" alt="avatar">';
                            }
                          ?>
                        </div>
                        <label for="avatar" class="custom-file-upload"><i class="fa fa-cloud-upload"></i>Chọn ảnh</label>
                        <input type="file" id="avatar" name="avatar" class="imgInp bg-success text-white" accept="image/png, image/jpeg">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="address">Địa chỉ</label>
                      <input type="text" name="address" id="address" class="form-control" placeholder="Địa chỉ" value="<?php echo isset($_COOKIE["address"]) ? $_COOKIE["address"] : "" ?>"/>
                    </div>
                    <div class="form-group sp50">
                      <label for="birthday">Ngày sinh</label>
                      <input type="date" name="birthday" id="birthday" class="form-control" placeholder="Ngày sinh" value="<?php echo isset($_COOKIE["birthday"]) ? explode(" ", $_COOKIE["birthday"])[0] : "" ?>"/>
                    </div>
                    <div class="form-group sp50 sp50_last">
                      <label for="phone">Số điện thoại</label>
                      <input type="text" name="phone" id="phone" class="form-control" placeholder="Số điện thoại" value="<?php echo isset($_COOKIE["phone"]) ? $_COOKIE["phone"] : "" ?>"/>
                    </div>
                    <div class="form-group sp_clear">
                      <label for="email">Email</label>
                      <input type="email" name="email" id="email" class="form-control" placeholder="Email" value="<?php echo isset($_COOKIE["email"]) ? $_COOKIE["email"] : "" ?>"/>
                    </div>
                    <div class="form-group grp-submit">
                      <button type="submit" class="btn btn-primary btn-submit">
                        <span class="fa fa-plus"></span>
                        Cập nhật
                      </button>
                      <button type="button" class="btn btn-close-submit">
                        <span class="fa fa-times"></span>
                        Hủy bỏ
                      </button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="copyright_info">
        <span class="count_timing">00:00:00 00/00/0000</span>
        <span>copyright © (phuoc nhi) 2020</span>
      </div>
    </div>
  </div>

  <script src="../../assets/js/user.js"></script>
  <script>
  $(document).ready(function() {
    updateProfile($("#user-frm"));
  });
  </script>
</body>

</html>