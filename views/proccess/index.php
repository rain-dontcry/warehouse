<?php include '../../includes/header.php' ?>

<body class="mat-typography">
  <?php include '../../includes/navbar.php' ?>
  <div id="wrapper" class="wrapper">
    <?php include '../../includes/sidebar.php' ?>
    <div class="content">
      <div class="breadcrumb">
        <?php include '../../includes/breadcrumb.php';?>
        <?php
          $breadcrumb = new breadcrumb();
          echo $breadcrumb->build(array(
            'Phân bổ' => '/views/proccess/'
          ));
        ?>
        <div class="search-form">
          <form [formGroup]="searchFrm" (ngSubmit)="onSubmit(searchFrm.value)">
            <input type="text" name="searchKey">
            <span class="ico-search fa fa-search"></span>
          </form>
        </div>
      </div>
      <div class="main-content">
        <div class="page-title">
          <h1>Phân bổ công việc</h1>
        </div>
        
        <div class="page-wrapper">
          <div class="page-content">
            <div class="row">
              <div class="col-3 sp_hide form_modal">
                <form id="product-frm" class="js-binding-frm" data-action="../../controller/proccess/insert.php" data-update="../../controller/proccess/update.php">
                  <button type="button" class="pc_hide md_close">Close</button>
                  <input type="hidden" name="id" id="id" class="form-control" placeholder="id" />
                  <div class="form-group">
                    <label for="username">Nhân viên</label>
                    <select name="username" class="form-control" id="username" required>
                      <option value="">Nhân viên--</option>
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="location">Kho phụ trách</label>
                    <select name="location" id="location" class="form-control" required>
                      <option value="">Kho phụ trách --</option>
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="status">Trạng thái</label>
                    <select name="status" id="status" class="form-control" required>
                      <option value="">Trạng thái --</option>
                    </select>
                  </div>
                  <div class="form-group grp-submit">
                    <button type="submit" class="btn btn-primary btn-submit">
                      <span class="fa fa-plus"></span>
                      Thêm trường mới
                    </button>
                    <button class="btn btn-close-submit" type="button">
                      <span class="fa fa-times"></span>
                      Hủy bỏ
                    </button>
                  </div>
                </form>
              </div>
              <div class="col-7">
                <div class="product-list">
                  <table class="product-list-tbl js-getlist display" data-list="../../controller/proccess/getlist.php">
                    <thead>
                      <tr>
                        <th>Nhân viên</th>
                        <th>Kho phụ trách</th>
                        <th>Trạng thái</th>
                        <th class="col_edit">Edit</th>
                      </tr>
                    </thead>
                    <tbody></tbody>
                  </table>
                  <script id="theTmpl" type="text/x-jsrender">
                    <tr data-edit="{`id`:`{{:id}}`,`username`:`{{:username}}`,`location`:`{{:location}}`,`status`:`{{:status}}`}">
                      <td>{{:username}}</td>
                      <td>{{:location}}</td>
                      <td>{{:status}}</td>
                      <td class="center">
                        <span class="btn-table btn-edit fa fa-pencil-square-o" aria-hidden="true" data-edit="{`id`:`{{:id}}`,`username`:`{{:username}}`,`location`:`{{:location}}`,`status`:`{{:status}}`}"></span>
                        <span class="btn-table btn-delete fa fa-trash-o" aria-hidden="true" data-delete="../../controller/proccess/delete.php?id={{:id}}&name={{:username}}"></span>
                      </td>
                    </tr>
                  </script>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="copyright_info">
        <span class="count_timing">00:00:00 00/00/0000</span>
        <span>copyright © (phuoc nhi) 2020</span>
      </div>
    </div>
  </div>

  <div class="import_loading">
    <div class="loading_container">
      <div class="loader">Loading...</div>
      <span class="status_import">Loading...</span>
    </div>
  </div>

  <script>
  if(!adminRoles()) {
    window.location.replace("/");
  }
  $(document).ready(function() {
    $('.import_loading').fadeIn(function() {
      var _table = $('.js-getlist');
      getList(_table);
      httpRequest($("#product-frm"), 'POST', _table);

      // load user(nhân viên)
      $.ajax({
        type: "get",
        url: "/controller/user/getlist.php",
        success: function (res) {
          var _sel = '';
          for (let i = 0; i < res.length; i++) {
            if(res[i].roles == 'user') {
              _sel += '<option value="' + res[i].id + '">' + res[i].username + '</option>';
            }
          }
          $('#username').append($(_sel));
        }
      });

      // load trang thái
      $.ajax({
        type: "get",
        url: "/controller/status/getlist.php",
        success: function (res) {
          var _sel = '';
          for (let i = 0; i < res.length; i++) {
            _sel += '<option value="' + res[i].id + '">' + res[i].status + '</option>';
          }
          $('#status').append($(_sel));
        }
      });

      // load Kho
      $.ajax({
        type: "get",
        url: "/controller/product/getlist.php",
        success: function (res) {
          var _sel = [];
          var _opt = '';
          for (let i = 0; i < res.length; i++) {
            if(res[i].location.includes('-')) {
              var _loc = res[i].location.split('-')[0];
              if(!_sel.includes(_loc)) {
                _sel.push(_loc);
              }
            }
          }
          for (let i = 0; i < _sel.length; i++) {
            _opt += '<option value="' + _sel[i] + '">' + _sel[i] + '</option>';
          }
          $('#location').append($(_opt));
        }
      });
    });
  });
  </script>
</body>

</html>