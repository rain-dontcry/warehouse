<?php include '../../includes/header.php' ?>

<body class="mat-typography">
  <?php include '../../includes/navbar.php' ?>
  <div id="wrapper" class="wrapper">
    <?php include '../../includes/sidebar.php' ?>
    <div class="content">
      <div class="breadcrumb">
        <?php include '../../includes/breadcrumb.php';?>
        <?php
          $breadcrumb = new breadcrumb();
          echo $breadcrumb->build(array(
            'Danh sách sản phẩm' => '/views/sanpham/'
          ));
        ?>
        <div class="search-form">
          <form [formGroup]="searchFrm" (ngSubmit)="onSubmit(searchFrm.value)">
            <input type="text" name="searchKey">
            <span class="ico-search fa fa-search"></span>
          </form>
        </div>
      </div>
      <div class="main-content">
        <div class="page-title">
          <h1>Count</h1>
          <button type="button" class="btn bg-danger text-white js_clear_list js_roles" data-clear="../controller/product/clearall.php"><span class="fa fa-trash-o"></span> Clear all</button>
          <div class="grp-image js_roles">
            <label for="import_product" class="custom-file-upload"><i class="fa fa-cloud-upload"></i>Nhập sản phẩm</label>
            <input type="file" id="import_product" onchange="parseExcel(this)" class="imgInp bg-success text-white">
          </div>
        </div>
        
        <div class="page-wrapper">
          <div class="page-content">
            <div class="row">
              <div class="col-3 sp_hide form_modal">
                <form id="product-frm" class="js-binding-frm" data-action="../../controller/product/insert.php" data-update="../../controller/product/update.php">
                  <button type="button" class="pc_hide md_close">Close</button>
                  <input type="hidden" name="id" id="id" class="form-control" placeholder="id" />
                  <div class="form-group sp50">
                    <label for="barcode">Barcode</label>
                    <input type="text" name="barcode" id="barcode" class="form-control" placeholder="Barcode" required />
                  </div>
                  <div class="form-group sp50 sp50_last">
                    <label for="sku">Mã sản phẩm</label>
                    <input type="text" name="sku" id="sku" class="form-control" placeholder="Mã sản phẩm" required />
                  </div>
                  <div class="form-group sp_clear">
                    <label for="name">Tên sảm phẩm</label>
                    <input type="text" name="name" id="name" class="form-control" placeholder="Tên sản phẩm" required />
                  </div>
                  <div class="form-group">
                    <label for="location">Vị trí</label>
                    <input type="text" name="location" id="location" class="form-control" placeholder="Vị trí" />
                  </div>
                  <div class="form-group sp50">
                    <label for="count">Số lượng</label>
                    <input type="number" name="count" id="count" class="form-control" placeholder="Số lượng" autocomplete="off" />
                  </div>
                  <div class="form-group sp50 sp50_last">
                    <label for="qty_return">Qty return</label>
                    <input type="number" name="qty_return" id="qty_return" class="form-control" placeholder="Qty return" disabled/>
                  </div>
                  <div class="form-group grp-submit sp_clear">
                    <button type="submit" class="btn btn-primary btn-submit">
                      <span class="fa fa-plus"></span>
                      Thêm trường mới
                    </button>
                    <button class="btn btn-close-submit" type="button">
                      <span class="fa fa-times"></span>
                      Hủy bỏ
                    </button>
                  </div>
                </form>
              </div>
              <div class="col-7">
                <div class="product-list">
                  <table class="product-list-tbl js-getlist" data-list="../../controller/product/getlist.php">
                    <thead>
                      <tr>
                        <th>Barcode</th>
                        <th>Mã sản phẩm</th>
                        <th class="name_pro">Tên sản phẩm</th>
                        <th class="location_pro">Vị trí</th>
                        <th>Count</th>
                        <th class="js_roles">Edit</th>
                      </tr>
                    </thead>
                    <tbody></tbody>
                  </table>
                  <script id="theTmpl" type="text/x-jsrender">
                    <tr data-edit="{`id`:`{{:id}}`,`barcode`:`{{:barcode}}`,`sku`:`{{:sku}}`,`name`:`{{:name}}`,`location`:`{{:location}}`,`onhand`:`{{:onhand}}`,`allocated`:`{{:allocated}}`,`count`:`{{:count}}`,`qty_return`:``}">
                      <td>{{:barcode}}</td>
                      <td>{{:sku}}</td>
                      <td>{{:name}}</td>
                      <td>{{:location}}</td>
                      <td>{{:count}}</td>
                      <td class="center js_roles">
                        <div class="tbl_button">
                          <span class="btn-table btn-edit fa fa-pencil-square-o" aria-hidden="true" data-edit="{`id`:`{{:id}}`,`barcode`:`{{:barcode}}`,`sku`:`{{:sku}}`,`name`:`{{:name}}`,`location`:`{{:location}}`,`onhand`:`{{:onhand}}`,`allocated`:`{{:allocated}}`,`count`:`{{:count}}`,`qty_return`:`0`}"></span>
                          <span class="btn-table btn-delete fa fa-trash-o" aria-hidden="true" data-delete="../../controller/product/delete.php?id={{:id}}&name={{:name}}"></span>
                        </div>
                      </td>
                    </tr>
                  </script>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="copyright_info">
        <span class="count_timing">00:00:00 00/00/0000</span>
        <span>copyright © (phuoc nhi) 2020</span>
      </div>
    </div>
  </div>

  <div class="import_loading">
    <div class="loading_container">
      <div class="loader">Loading...</div>
      <span class="status_import">Loading...</span>
    </div>
  </div>

  <script>
  $(document).ready(function() {
    $('.import_loading').fadeIn(function() {
      var _table = $('.js-getlist');
      getList(_table);
      httpRequest($("#product-frm"), 'POST', _table);
    });
  });
  </script>
</body>

</html>