function updateProfile(frm) {
  frm.submit(function (e) {
    e.preventDefault();
    var form = $(this);
    var url = form.data('action');
    var formData = new FormData(this);
    if(!formData.get('avatar').name) {
      formData.delete('avatar');
    }

    $.ajax({
      type: "POST",
      url: url,
      contentType: "application/json; charset=utf-8",
      dataType: "json",
      data: formData, // serializes the form's elements.
      cache:false,
      contentType: false,
      processData: false,
      success: function (data) {
        // console.log(formData);
        if (data) {
          setCookie('firstname', formData.get('firstname', 1));
          setCookie('lastname', formData.get('lastname', 1));
          // setCookie('avatar', formData.get('avatar', 1));
          setCookie('address', formData.get('address', 1));
          setCookie('birthday', formData.get('birthday', 1));
          setCookie('phone', formData.get('phone', 1));
          setCookie('email', formData.get('email', 1));
          location.reload();
        }
      }
    });
  });
}

$(document).ready(function () {
  $('.js_update_profile').click(function() {
    $('.js_edit_profile').fadeIn();
  });
  $('.md_close, .js_edit_profile .btn-close-submit').click(function() {
    $(this).parents('.js_edit_profile').fadeOut();
  });
});
