jQuery(function ($) {

  // navbar user
  var dropUser = $('.js-drop_user');
  var userMenu = $('.js-drop_user_hint');
  function isIOS() {
    return /iPad|iPhone|iPod/.test(navigator.platform);
  }
  var eventDropdownRoute = isIOS() ? 'touchstart' : 'click';
  $(document).on(eventDropdownRoute, function (e) {
    if (dropUser.is(e.target) || dropUser.has(e.target).length) {
      dropUser.toggleClass('is_active');
      userMenu.slideToggle();
    } else if (!userMenu.is(e.target) && userMenu.has(e.target).length === 0) {
      dropUser.removeClass('is_active');
      userMenu.slideUp();
    }
  });


  // $('body').on('click', '.top-menu .dropdown-item', function () {
  //   $(this).find('.menu-dropdown').slideToggle('fast');
  // });

  // sidebar
  $('body').on('click', '.dropdown-item > span', function () {
    $(this).parent('.dropdown-item').toggleClass('open');
    $(this).parent('.dropdown-item').find('.menu-dropdown').slideToggle();
  });

  // proccess sidebar with url
  var _url = window.location.href;
  var _page = '';
  if (_url.includes('views/')) {
    _page = _url.split('views/')[1];
  }
  if (_page) {
    $('.sidebar .menu-bar li').each(function () {
      var _match = $(this).find('a').attr('href');
      if (_match.includes('views/')) {
        _match = _match.split('views/')[1];
        if (_match == _page) {
          $(this).addClass('active');
          if ($(this).parents('.dropdown-item').length) {
            $(this).parents('.dropdown-item').addClass('active');
          }
        }
      }
    });
  } else {
    $('.sidebar .menu-bar li').first().addClass('active');
  }

  // proccess for input image
  function readURL(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = function (e) {
        $('.img-view').attr('src', e.target.result);
      }
      reader.readAsDataURL(input.files[0]); // convert to base64 string
    }
  }

  $(".imgInp").change(function () {
    readURL(this);
  });

  // process breadcrumb on mobile
  $('ol.bread').clone().appendTo('.breadcrumb-mobile');

  $('.tg-menu-mobile').click(function () {
    if ($(this).hasClass('open')) {
      $(this).removeClass('open');
      $(this).animate({ left: "5px" }, { duration: 600, queue: false });
      $('.sidebar').animate({ left: "-255px" }, { duration: 600, queue: false });
    } else {
      $(this).addClass('open');
      $(this).animate({ left: "210px" }, { duration: 600, queue: false });
      $('.sidebar').animate({ left: "0" }, { duration: 600, queue: false });
    }
  });

  confirmDelete();
  bindingEdit();
  $('.form_modal .md_close, .btn-close-submit').click(function () {
    $('.form_modal').fadeOut(function () {
      resetFrm();
    });
  });
  clearList();
  runClock();

  $('.js_sp_add').click(function() {
    resetFrm();
    $('.form_modal').fadeIn();
  });

});

var is_update = false;
var _tableAfterBinding;
var _rowToEdit;

function adminRoles() {
  if(getCookie('roles') == 'admin') {
    return true;
  }
  return false;
}

// cookie
function setCookie(cname, cvalue, exdays) {
  var d = new Date();
  d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
  var expires = "expires=" + d.toUTCString();
  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
  var name = cname + "=";
  var ca = document.cookie.split(';');
  for (var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}

function clearCookie(name) {
  document.cookie = name + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
}

function checkCookie(cookie_name) {
  return getCookie(cookie_name);
}

function getList(_tableDta) {
  var _dataList = _tableDta.data('list');
  if(_tableAfterBinding) {
    _tableDta.dataTable().fnDestroy();
  }
  _tableDta.find('tbody').empty();
  $.ajax({
    type: "GET",
    url: _dataList,
    success: function (res) {
      var template = $.templates("#theTmpl");
      var htmlOutput = template.render(res);
      _tableDta.find('tbody').append(htmlOutput);
      if(getCookie('roles') != 'admin') {
        $('.js_roles').remove();
      }

      if(window.location.href.split('views/')[1] == 'status/') {
        _tableAfterBinding = _tableDta.DataTable({
          "lengthMenu": [[12, 20, 50, -1], [12, 20, 50, 'All']],
          ordering: false
        });
      } else {
        _tableAfterBinding = _tableDta.DataTable({
          "lengthMenu": [[12, 20, 50, -1], [12, 20, 50, 'All']],
          dom: 'Bfrtip',
          buttons: [
            {
              extend: 'excel',
              text: 'Xuất file excel'
            }
          ]
        });
      }
    }
  })
  .always(function() {
    $('.import_loading').fadeOut();
  });
};

function confirmDelete() {
  $('body').on('click', '.btn-delete', function () {
    var itemDelete = $(this).data('delete');
    var _tableDta = $(this).parents('table');
    var name = itemDelete.split('name=')[1].split('&')[0];
    if (confirm('Bạn có muốn xóa trường ' + name + '?')) {
      $.ajax({
        type: "GET",
        url: itemDelete,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
          data = JSON.parse(data);
          if ((data.res == '1') && (_tableDta)) {
            getList(_tableDta);
            if($(window).width() < 650) {
              $('.form_modal').hide();
            }
            resetFrm();
          }
        }
      });
    } else {
      $('.form_modal').hide();
    }
  });
}

function clearList() {
  $('body').on('click', '.js_clear_list', function () {
    var itemDelete = $(this).data('clear');
    if (confirm('Bạn có muốn xóa hết dữ liệu hiện có?')) {
      $.ajax({
        type: "GET",
        url: itemDelete,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
          data = JSON.parse(data);
          if (data.res == '1') {
            location.reload();
          }
        }
      });
    } else {
    }
  });
}

function bindingEdit() {
  var _disableData = ['username', 'password', 're_password', 'barcode', 'sku', 'name', 'location', 'onhand', 'allocated'];
  var _elChoose;
  if((window.location.href.split('views/')[1] == 'user/') && (getCookie('roles') != 'admin')) {
    _elChoose = '.btn-edit';
  } else {
    _elChoose = '.btn-edit, .js-getlist tr:not(:first)';
  }
  $('body').on('click', _elChoose, function () {
    _rowToEdit = _tableAfterBinding.row(this);
    if ($(window).width() < 650) {
      $('.form_modal').fadeIn();
    }
    let itemEdit = $(this).data('edit');
    var dataBinding = JSON.parse(itemEdit.replace(/`/g, '"'));
    var _frmBinding = $('.js-binding-frm');
    for (let i = 0; i < _disableData.length; i++) {
      let _frmKey = _frmBinding.find("input[name='" + _disableData[i] + "']");
      if (_frmKey.length) {
        _frmKey.prop('disabled', true);
      }
    }
    $.each(dataBinding, function (key, value) {
      var _elFrm = _frmBinding.find("input[name='" + key + "']");
      if (_elFrm.prop('type') == 'date') {
        _frmBinding.find("input[name='" + key + "']").val(value.replace('/', '-'));
      } else if(_elFrm.prop('type') == 'file') {
        _frmBinding.find(".img-view").attr('src', value);
      } else {
        _frmBinding.find("select[name='" + key + "']").val(value);
        _frmBinding.find("input[name='" + key + "']").val(value);
      }
    });
    _frmBinding.find('button[type="submit"]').html('<span class="fa fa-pencil-square-o" aria-hidden="true"></span>Cập nhật');
    is_update = true;
  });
}

function resetFrm() {
  var _frm = $('.js-binding-frm');
  // reset form
  _frm.find("input, textarea").val("");
  _frm.find('button[type="submit"]').html('<span class="fa fa-plus"></span>Thêm trường mới');
  $('img.img-view').attr('src', '../../assets/img/no-image.png');
  $('input').prop('disabled', false);
  $('select').val('');
  is_update = false;
}

// request http
function httpRequest(frm, method, _tableDta) {
  frm.submit(function (e) {
    e.preventDefault();
    var form = $(this);
    var url = is_update ? form.data('update') : form.data('action');

    var formData = new FormData(this);
    if(formData.get('avatar')) {
      if(!formData.get('avatar').name) {
        formData.delete('avatar');
      }
    }

    $.ajax({
      type: method,
      url: url,
      contentType: "application/json; charset=utf-8",
      dataType: "json",
      data: formData, // serializes the form's elements.
      cache:false,
      contentType: false,
      processData: false,
      success: function (data) {
        if((typeof data) != 'object') {
          data = JSON.parse(data);
        }
        if (((data.res == '1') && (_tableDta)) || data.id) {
          if ($(window).width() <= 650) {
            $('.form_modal').fadeOut();
          }
          // Edit data on row
          if(is_update) {
            let _dataOnRow = _rowToEdit.data();
            let _nodeRow = JSON.parse(_rowToEdit.node().getAttribute('data-edit').toString().replace(/`/g, '"'));
            _rowToEdit.node().setAttribute('data-edit', JSON.stringify(_nodeRow).replace(/"/g, '\''));
            var _count = form.find('input[id="count"]').val();
            var _qty_return = form.find('input[id="qty_return"]').val();
            var _new_location = form.find('input[id="new_location"]').val();
            _nodeRow.count = _count;
            _nodeRow.qty_return = '0';
            if(window.location.href.split('views/')[1] == 'sanpham/') {
              _dataOnRow[4] = _new_location;
              let _col8 = parseInt(_qty_return ? _qty_return : 0) + parseInt(_dataOnRow[8] ? _dataOnRow[8] : 0);
              _dataOnRow[8] = _col8;
            } else if (window.location.href.split('views/')[1] == 'count/') {
              let _col4 = _count;
              _dataOnRow[4] = _col4;
            }
            _tableAfterBinding.row(_rowToEdit).data(_dataOnRow).draw();
            var _url = window.location.href.split('views/')[1];
            if((_url == 'user/') || (_url == 'status/')) {
              getList(_tableDta);
            }
          } else {
            getList(_tableDta);
          }
        }
        resetFrm();
      }
    });
  });
}

var workbook = '';
// import product
function parseExcel(inputElement) {
  var files = inputElement.files || [];
  if (!files.length) return;
  var file = files[0];

  // show loading
  $('.import_loading').fadeIn();

  // console.time();
  var reader = new FileReader();
  reader.onloadend = function (event) {
    var arrayBuffer = reader.result;
    // debugger

    var options = { type: 'array' };
    workbook = XLSX.read(arrayBuffer, options);
    console.log(XLSX.utils.sheet_to_json(workbook[0], {
      raw: true
    }));
    // console.timeEnd();
    saveExcelToData();
  };
  reader.readAsArrayBuffer(file);
}

var _oneSheet = '';
var _sheet = 0;
var _score = 1;
var _total = 0;
var _count = 0;
function saveExcelToData() {
  _oneSheet = XLSX.utils.sheet_to_json(workbook.Sheets[workbook.SheetNames[_sheet]], { raw: true });
  _total += _oneSheet.length;
  $('.import_loading').find('.status_import').text('0 / ' + _total.toString());
  saveOneScore();
}

function saveOneScore() {
  let _oneScore = _oneSheet[_score];
  console.log(_oneScore);
  let _jsonScore = {
    'barcode': _oneScore['__EMPTY'] ? _oneScore['__EMPTY'] : '',
    'sku': _oneScore['__EMPTY_1'] ? _oneScore['__EMPTY_1'] : '',
    'name': _oneScore['__EMPTY_2'] ? _oneScore['__EMPTY_2'] : '',
    'location': _oneScore['__EMPTY_3'] ? _oneScore['__EMPTY_3'] : '',
    'onhand': _oneScore['__EMPTY_4'] ? _oneScore['__EMPTY_4'] : '',
    'allocated': _oneScore['__EMPTY_5'] ? _oneScore['__EMPTY_5'] : '0',
    'count': '',
    'qty_return': ''
  };

  $.ajax({
    type: "POST",
    url: "../controller/product/import.php",
    data: _jsonScore,
    dataType: "json",
    success: function (data) {
      data = JSON.parse(data);
      _score++;
      _count++;
      $('.import_loading').find('.status_import').text((_count + 1).toString() + ' / ' + _total.toString());
      if ((data.res == '1') && (_score < _oneSheet.length)) {
        saveOneScore();
      } else if (data.res == '0') {
        console.log(data);
      } else if ((_score >= _oneSheet.length - 1) && (_sheet >= workbook.SheetNames.length - 1)) {
        $('.import_loading').fadeOut();
        getList($('.js-getlist'));
        return;
      } else {
        _sheet++;
        if (_sheet < workbook.SheetNames.length) {
          _score = 1;
          saveExcelToData();
        }
      }
    }
  }).fail(function (e) {
    console.log(e);
  });
}

function runClock() {
  var d = new Date();
  var _timeString = d.getHours() + ':' + d.getMinutes().toString().padStart(2, '0') + ':' + d.getSeconds().toString().padStart(2, '0') + ' ' + d.getDate() + '/' + (d.getMonth() + 1).toString().padStart(2, '0') + '/' + d.getFullYear();
  $('.count_timing').text(_timeString);
  setTimeout(function () {
    runClock();
  }, 1000);
}