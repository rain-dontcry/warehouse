<?php
class Update {
	// database connection and table name
	private $conn;
	private $tableName = "users";

  // object properties
  public $id;
  public $firstname;
  public $lastname;
  public $address;
  public $birthday;
  public $phone;
  public $email;
  public $avatar;
  public $username;
  public $password;
  public $re_password;
  public $created;
	public $modified;

	public function __construct($db) {
		$this->conn = $db;
	}

	// Update users
	function init() {
    $_tableName = $this->tableName;
    $_firstname = $this->firstname ? "'" . $this->firstname . "'" : '';
		$_lastname = $this->lastname ? "'" . $this->lastname . "'" : '';
		$_address = $this->address ? "'" . $this->address . "'" : '';
		$_birthday = $this->birthday ? "'" . $this->birthday . "'" : '';
		$_phone = $this->phone ? "'" . $this->phone . "'" : '';
    $_email = $this->email ? "'" . $this->email . "'" : '';
		$_avatar = $this->avatar ? "'" . $this->avatar . "'" : '';
		$_password = $this->password ? "'" . md5($this->password) . "'" : '';
		$_modified = $this->modified;
		
		$query = "UPDATE " . $_tableName . " SET ";
		if($_firstname) {
			$query .= "`firstname`=" . $_firstname . ",";
		} else {
			$query .= "`firstname`='',";
		}
		if($_lastname) {
			$query .= "`lastname`=" . $_lastname . ",";
		} else {
			$query .= "`lastname`='',";
		}
		if($_address) {
			$query .= "`address`=" . $_address . ",";
		} else {
			$query .= "`address`='',";
		}
		if($_birthday) {
			$query .= "`birthday`=" . $_birthday . ",";
		} else {
			$query .= "`birthday`=NULL,";
		}
		if($_phone) {
			$query .= "`phone`=" . $_phone . ",";
		} else {
			$query .= "`phone`='',";
		}
		if($_email) {
			$query .= "`email`=" . $_email . ",";
		} else {
			$query .= "`email`='',";
		}
		if($_avatar) {
			$query .= "`avatar`=" . $_avatar . ",";
		}
		if($_password) {
			$query .= "`password`=" . $_password . ",";
		}
		$query .= "`modified`='" . $_modified . "' WHERE `id`=" . $this->id;

		if($_avatar) {
			// delete avatar
			$queryDel = "SELECT `avatar` FROM " . $_tableName . " WHERE `id`=" . $this->id;
			$stmt = $this->conn->prepare($queryDel);
			$stmt->execute();
			// check if more than 0 record found
			$num = $stmt->rowCount();
			if ($num == 1) {
				while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
					extract($row);
					$dirTmp = rtrim($_SERVER['DOCUMENT_ROOT'], "/");

					if($avatar) {
						if (!unlink($dirTmp . $avatar)) {
							echo json_encode('{"res": "0"}');
							die;
						}
					}
				}
			} else {
				return 0;
			}
		}

		// prepare query
		$stmt = $this->conn->prepare($query);

		// execute query
		if ($stmt->execute()) {
			$query = "SELECT * FROM " . $_tableName . " WHERE `id`=" . $this->id;
			// prepare query
			$stmt = $this->conn->prepare($query);
			// execute query
			$stmt->execute();
			return $stmt;
		}

		return false;
	}

}

?>