<?php
class Delete {

	// database connection and table name
	private $conn;
	private $tableName = "users";

  // object properties
  public $id;

	public function __construct($db) {
		$this->conn = $db;
	}

	// insert Delete
	function init() {
    $_tableName = $this->tableName;
    $_id = $this->id;
    // query to insert record
    $query = "DELETE FROM " . $_tableName . " WHERE id='" . $_id . "'";

		// prepare query
		$stmt = $this->conn->prepare($query);

		// execute query
    $stmt->execute();
    return $stmt;
	}

}

?>