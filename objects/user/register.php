<?php
class Register {

  // database connection and table name
  private $conn;
  private $tableName = "users";

  // object properties
  public $firstname;
  public $lastname;
  public $address;
  public $birthday;
  public $phone;
  public $email;
  public $avatar;
  public $username;
  public $password;
  public $roles;
  public $re_password;
  public $created;
  public $modified;

  public function __construct($db) {
    $this->conn = $db;
  }

  // insert register
  function init() {
    $_tableName = $this->tableName;
    $_firstname = $this->firstname ? "'" . $this->firstname . "'" : 'NULL';
    $_lastname = $this->lastname ? "'" . $this->lastname . "'" : 'NULL';
    $_address = $this->address ? "'" . $this->address . "'" : 'NULL';
    $_birthday = $this->birthday ? "'" . $this->birthday . "'" : 'NULL';
    $_phone = $this->phone ? "'" . $this->phone . "'" : 'NULL';
    $_email = $this->email ? "'" . $this->email . "'" : 'NULL';
    $_avatar = $this->avatar ? "'" . $this->avatar . "'" : 'NULL';
    $_username = $this->username;
		$_password = md5($this->password);
		$_roles = $this->roles;
    $_created = $this->created;
    $_modified = $this->modified;
    // query to insert record
    $query = "INSERT INTO " . $_tableName . "(`firstname`, `lastname`, `address`, `birthday`, `phone`, `email`, `avatar`, `username`, `password`, `roles`, `created`, `modified`) VALUES (" . $_firstname . ", " . $_lastname . ", " . $_address . ", " . $_birthday . ", " . $_phone . ", " . $_email . ", " . $_avatar . ", '" . $_username . "', '" . $_password . "', '" . $_roles . "', '" . $_created . "', '" . $_modified . "')";
    // echo $query;die;
    
    // prepare query
    $stmt = $this->conn->prepare($query);

    // execute query
    if ($stmt->execute()) {
      return true;
    }

    return false;
  }

}

?>