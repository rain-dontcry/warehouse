<?php
class Getlist {

	// database connection and table name
	private $conn;
	private $tableName = "users";

  // object properties
  public $username;
  public $firstname;
  public $lastname;
  public $avatar;
  public $address;
  public $birthday;
  public $phone;
  public $email;
  public $roles;
  public $created;
  public $modified;

	public function __construct($db) {
		$this->conn = $db;
	}

	// insert getlist
	function init() {
    $_tableName = $this->tableName;
    // query to insert record
    $query = "SELECT * FROM " . $_tableName;

		// prepare query
		$stmt = $this->conn->prepare($query);

		// execute query
    $stmt->execute();
    return $stmt;
	}

}

?>