<?php
class Getlist {

	// database connection and table name
	private $conn;

  // object properties
  public $id;
  public $username;
  public $location;
  public $status;
  public $created;
  public $modified;

	public function __construct($db) {
		$this->conn = $db;
	}

	// insert getlist
	function init() {
    // query to insert record
    $query = "SELECT c.id, a.username as username, c.location_pro as location, b.status
    FROM `users` a, `status` b, `status_count` c
    WHERE a.id=c.id_user AND b.id=c.id_status";

		// prepare query
		$stmt = $this->conn->prepare($query);

		// execute query
    $stmt->execute();
    return $stmt;
	}

}

?>