<?php
class Insert {

  // database connection and table name
  private $conn;
  private $tableName = "status";

  // object properties
  public $status;
  public $order;
  public $created;
  public $modified;

  public function __construct($db) {
    $this->conn = $db;
  }

  // insert insert
  function init() {
    $_tableName = $this->tableName;
    $_status = $this->status ? "'" . $this->status . "'" : 'NULL';
    $_order = $this->order ? "'" . $this->order . "'" : 'NULL';
    $_created = $this->created;
    $_modified = $this->modified;
    // query to insert record
    $query = "INSERT INTO " . $_tableName . "(`status`, `order`, `created`, `modified`) VALUES (" . $_status . ", " . $_order . ", '" . $_created . "', '" . $_modified . "')";
    
    // prepare query
    $stmt = $this->conn->prepare($query);

    // execute query
    if ($stmt->execute()) {
      return true;
    }

    return false;
  }

}

?>