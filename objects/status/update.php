<?php
class Update {
	// database connection and table name
	private $conn;
	private $tableName = "status";

  // object properties
  public $id;
  public $status;
  public $order;
  public $modified;

	public function __construct($db) {
		$this->conn = $db;
	}

	// Update Products
	function init() {
		$_tableName = $this->tableName;
    $_status = $this->status ? "'" . $this->status . "'" : '';
    $_order = $this->order ? "'" . $this->order . "'" : '';
		$_modified = $this->modified;
		
		$query = "UPDATE " . $_tableName . " SET ";
		if($_status) {
			$query .= "`status`=" . $_status . ",";
    }
    if($_order) {
			$query .= "`order`=" . $_order . ",";
		}
		$query .= "`modified`='" . $_modified . "' WHERE `id`=" . $this->id;

		// prepare query
		$stmt = $this->conn->prepare($query);

		// execute query
		if ($stmt->execute()) {
			return true;
		}

		return false;
	}

}

?>