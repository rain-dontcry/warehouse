<?php
class Getlist {

	// database connection and table name
	private $conn;
	private $tableName = "status";

  // object properties
  public $id;
  public $status;
  public $created;
  public $modified;

	public function __construct($db) {
		$this->conn = $db;
	}

	// insert getlist
	function init() {
    $_tableName = $this->tableName;
    // query to insert record
		$query = "SELECT * FROM " . $_tableName . " ORDER BY `order`";

		// prepare query
		$stmt = $this->conn->prepare($query);

		// execute query
    $stmt->execute();
    return $stmt;
	}

}

?>