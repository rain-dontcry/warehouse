<?php
class Clearall {

	// database connection and table name
	private $conn;
	private $tableName = "products";

	public function __construct($db) {
		$this->conn = $db;
	}

	// insert Clearall
	function init() {
    $_tableName = $this->tableName;
    // query to insert record
    $query = "DELETE FROM " . $_tableName . ";ALTER TABLE " . $_tableName . " MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1";

    // echo $query;die;

		// prepare query
		$stmt = $this->conn->prepare($query);

		// execute query
    $stmt->execute();
    return $stmt;
	}

}

?>