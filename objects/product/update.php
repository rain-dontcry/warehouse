<?php
class Update {
	// database connection and table name
	private $conn;
	private $tableName = "products";

  // object properties
  public $id;
  public $barcode;
  public $sku;
  public $name;
  public $location;
  public $new_location;
  public $onhand;
  public $allocated;
	public $count;
	public $qty_return;
  public $modified;

	public function __construct($db) {
		$this->conn = $db;
	}

	// Update Products
	function init() {
		$_tableName = $this->tableName;
    $_barcode = $this->barcode ? "'" . $this->barcode . "'" : '';
		$_sku = $this->sku ? "'" . $this->sku . "'" : '';
		$_name = $this->name ? "'" . $this->name . "'" : '';
		$_location = $this->location ? "'" . $this->location . "'" : '';
		$_new_location = $this->new_location ? "'" . $this->new_location . "'" : '';
		$_onhand = $this->onhand ? "'" . $this->onhand . "'" : '';
    $_allocated = $this->allocated ? "'" . $this->allocated . "'" : '';
		$_count = $this->count ? "'" . $this->count . "'" : '';
		$_qty_return = $this->qty_return ? $this->qty_return : '';
		$_modified = $this->modified;
		
		// Get qty return
		$qty_return0 = 0;
		$query0 = "SELECT `qty_return` FROM " . $_tableName . " WHERE `id`=" . $this->id;
		// prepare query
		$stmt0 = $this->conn->prepare($query0);
		// execute query
		$stmt0->execute();
		$num = $stmt0->rowCount();
		// check if more than 0 record found
		if ($num > 0) {
			$row = $stmt0->fetch(PDO::FETCH_ASSOC);
			extract($row);
			$qty_return0 = $qty_return;
		} else {
			$qty_return0 = 0;
		}
		
		$query = "UPDATE " . $_tableName . " SET ";
		if($_barcode) {
			$query .= "`barcode`=" . $_barcode . ",";
		}
		if($_sku) {
			$query .= "`sku`=" . $_sku . ",";
		}
		if($_name) {
			$query .= "`name`=" . $_name . ",";
		}
		if($_location) {
			$query .= "`location`=" . $_location . ",";
		}
		if($_new_location) {
			$query .= "`new_location`=" . $_new_location . ",";
		}
		if($_onhand) {
			$query .= "`onhand`=" . $_onhand . ",";
		}
		if($_allocated) {
			$query .= "`allocated`=" . $_allocated . ",";
		}
		if($_count) {
			$query .= "`count`=" . $_count . ",";
		}
		if($_qty_return) {
			$query .= "`qty_return`=" . ((int)$_qty_return + (int)$qty_return0) . ",";
		}
		$query .= "`modified`='" . $_modified . "' WHERE `id`=" . $this->id;

		// prepare query
		$stmt = $this->conn->prepare($query);

		// execute query
		if ($stmt->execute()) {
			return true;
		}

		return false;
	}

}

?>