<?php
class Insert {
	// database connection and table name
	private $conn;
	private $tableName = "products";

  // object properties
  public $barcode;
  public $sku;
  public $name;
  public $location;
  public $new_location;
  public $onhand;
  public $allocated;
  public $count;
  public $qty_return;
  public $created;
  public $modified;

	public function __construct($db) {
		$this->conn = $db;
	}

	// insert Products
	function init() {
    $_tableName = $this->tableName;
    $_barcode = $this->barcode ? "'" . $this->barcode . "'" : 'NULL';
		$_sku = $this->sku ? "'" . $this->sku . "'" : 'NULL';
		$_name = $this->name ? "\"" . $this->name . "\"" : 'NULL';
		$_location = $this->location ? "'" . $this->location . "'" : 'NULL';
		$_new_location = $this->new_location ? "'" . $this->new_location . "'" : 'NULL';
		$_onhand = $this->onhand ? "'" . $this->onhand . "'" : 'NULL';
    $_allocated = $this->allocated ? "'" . $this->allocated . "'" : '0';
		$_count = $this->count ? "'" . $this->count . "'" : 'NULL';
		$_qty_return = $this->qty_return ? "'" . $this->qty_return . "'" : 'NULL';
    $_created = $this->created;
    $_modified = $this->modified;
		// query to insert record
		$query = "INSERT INTO " . $_tableName . "(`barcode`, `sku`, `name`, `location`, `new_location`, `onhand`, `allocated`, `count`, `qty_return`, `created`, `modified`) VALUES (" . $_barcode . ", " . $_sku . ", " . $_name . ", " . $_location . ", " . $_new_location . ", " . $_onhand . ", " . $_allocated . ", " . $_count . ", " . $_qty_return . ", '" . $_created . "', '" . $_modified . "')";
		
		// prepare query
		$stmt = $this->conn->prepare($query);

		// execute query
		if ($stmt->execute()) {
			return true;
		} else {
			echo $query; die;
		}

		return false;
	}

}

?>