<?php
class Getlist {

	// database connection and table name
	private $conn;
	private $tableName = "products";

  // object properties
  public $id;
  public $barcode;
  public $sku;
  public $name;
  public $location;
  public $new_location;
  public $onhand;
  public $allocated;
  public $count;
  public $qty_return;
  public $created;
  public $modified;

	public function __construct($db) {
		$this->conn = $db;
	}

	// insert getlist
	function init() {
    $_tableName = $this->tableName;
    // query to insert record
    $query = "SELECT * FROM " . $_tableName;

		// prepare query
		$stmt = $this->conn->prepare($query);

		// execute query
    $stmt->execute();
    return $stmt;
	}

}

?>