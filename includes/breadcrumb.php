<?php
class breadcrumb
{
   private $breadcrumb;

   private $separator = ' / ';

   private $domain = 'http://localhost/warehouse';

   public function build($array)
   {
      $breadcrumbs = array_merge(array('home' => ''), $array);

      $count = 0;
      $this->breadcrumb .= '<ol class="bread">';

      foreach($breadcrumbs as $title => $link) {
         $this->breadcrumb .= '
         <li class="breadcrumb-item" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
            <a href="'.$this->domain. '/'.$link.'" itemprop="url">
               <span itemprop="title">'.$title.'</span>
            </a>
         </li>';

         $count++;

         if($count !== count($breadcrumbs)) {
            $this->breadcrumb .= $this->separator;
         }
      }
      $this->breadcrumb .= '</ol>';
      return $this->breadcrumb;
   }
}
?>