<nav class="top-navbar">
  <div class="logo">
    <a href="warehouse">
      <span class="logo-icon fa fa-home"></span>
      <span class="site-name">Warehouse</span>
    </a>
  </div>
  <div class="top-menu">
    <div class="menu">
      <div class="dropdown-item">
        <button class="btn btn-drop-user bg-success js-drop_user">
          <?php
            if($_COOKIE["avatar"]) {
              echo '<div class="avatar_nav"><img src="' . $_COOKIE["avatar"] . '" class="img-view" alt="avatar"></div>';
            } else {
              echo '<i class="fa fa-user" aria-hidden="true"></i>';
            }
          ?>
          <span class="user-name"><?php $_COOKIE["username"] ? $_COOKIE["username"] : 'root' ?></span>
          <i class="fa fa-angle-down" aria-hidden="true"></i>
        </button>
        <div class="js-drop_user_hint menu-dropdown">
          <ul>
            <li>
              <a href="../views/user/profile.php">Profile</a>
            </li>
            <li>
              <a href="#">Settings</a>
            </li>
            <li>
              <a onclick="logout()">Logout</a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</nav>
<script>
if (!checkCookie('username')) {
  window.location.replace("../views/user/login.php");
}
if (checkCookie("username")) {
  $('.user-name').text(getCookie("username"));
}
var logout = function() {
  clearCookie('username');
  window.location.replace("../views/user/login.php");
};
</script>