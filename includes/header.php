<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <title>Warehouse</title>
  <base href="/">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" type="image/x-icon" href="./assets/img/favicon.png">
  <link href="https://fonts.googleapis.com/css2?family=Roboto+Condensed:wght@400;700&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="../assets/css/font-awesome-4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="../assets/css/app.css">
  <link rel="stylesheet" href="../assets/css/navbar.css">
  <link rel="stylesheet" href="../assets/css/sidebar.css">
  <link rel="stylesheet" href="../assets/css/breadcrumb.css">
  <link rel="stylesheet" href="../assets/css/user.css">
  <link rel="stylesheet" href="../assets/css/datatable.css">
  <link rel="stylesheet" href="../assets/css/mobile.css">
  <script src="../assets/js/jquery.min.js"></script>
  <script src="../assets/js/jsrender.min.js"></script>
  <script src="../assets/js/jquery.dataTables.min.js"></script>
  <script src="../assets/js/xlsx.full.min.js"></script>
  <script src="../assets/js/dataTables.buttons.min.js"></script> 
  <script src="../assets/js/jszip.min.js"></script>
  <script src="../assets/js/buttons.html5.min.js"></script>
  <script src="../assets/js/app.js"></script>
</head>