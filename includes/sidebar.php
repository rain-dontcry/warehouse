<div id="sidebar">
  <div class="breadcrumb-mobile pc_hide">
    <button class="tg-menu-mobile">
      <span class="tg-bar"></span>
      <span class="tg-bar"></span>
      <span class="tg-bar"></span>
    </button>
  </div>
  <div class="sidebar">
    <div class="quick-link">
      <a href="">
        <button class="bg-success text-white">
          <span class="fa fa-bar-chart"></span>
        </button>
      </a>
      <a href="../views/sanpham/">
        <button class="bg-info text-white">
          <span class="fa fa-briefcase"></span>
        </button>
      </a>
      <a href="../views/user/">
        <button class="bg-warning text-white">
          <span class="fa fa-users"></span>
        </button>
      </a>
      <a href="">
        <button class="bg-danger text-white">
          <span class="fa fa-cogs"></span>
        </button>
      </a>
    </div>
    <div class="sidebar-menu">
      <ul class="menu-bar">
        <li>
          <a href="/">
            <span class="ico-menu fa fa-tachometer"></span>
            <span>Dashboard</span>
          </a>
        </li>
        <li class="dropdown-item">
          <span>
            <span class="ico-menu fa fa-briefcase"></span>
            <span>Warehouse</span>
          </span>
          <ul class="menu-dropdown">
            <li>
              <a href="../views/sanpham/">
                <span>Return</span>
              </a>
            </li>
            <li>
              <a href="../views/count/">
                <span>Count</span>
              </a>
            </li>
            <?php
              if($_COOKIE["roles"] == "admin")
              echo '<li><a href="../views/proccess/"><span>Phân bổ</span></a></li><li><a href="../views/status/"><span>Trạng thái</span></a></li>';
            ?>
          </ul>
        </li>
        <?php
          if($_COOKIE["roles"] == "admin") {
            echo '<li>
                  <a href="../views/user/">
                    <span class="ico-menu fa fa-users"></span>
                    <span>Users</span>
                  </a>
                </li>';
          }
        ?>
      </ul>
      <button class="toggle-sidebar"><span class="fa fa-chevron-circle-left"></span></button>
    </div>
  </div>
</div>